from rest_framework import serializers

from geo.models import Address, City, Country, State


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        exclude = []


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        exclude = []


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        exclude = []


class AddressSerializer(serializers.ModelSerializer):
    city_data = CitySerializer(source="city", read_only=True)
    state_data = StateSerializer(source="state", read_only=True)

    class Meta:
        model = Address
        exclude = []
