from django.urls import include, path
from rest_framework import routers

from geo.views import CityListView, CountryListView, StateListView

app_name = 'geo'


def get_router():
    router = routers.DefaultRouter()
    router.register('country', CountryListView)
    router.register('state', StateListView)
    router.register('city', CityListView)
    return router


urlpatterns = [path('', include(get_router().urls))]
