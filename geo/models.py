from django.db import models


class Country(models.Model):
    """
    Country model to store country data.
    """

    name = models.CharField(max_length=50)
    code2 = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id', )


class State(models.Model):
    """
    State model to store state data for a country.
    """

    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)
    is_union_territory = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id', )


class City(models.Model):
    """
    City model to store city data for a given State.
    """

    name = models.CharField(max_length=50)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('id', )


class Address(models.Model):
    """
    Abstract Address Model to store Address data.
    """

    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True)
    line_one = models.TextField(help_text="Address Line 1", blank=True)
    line_two = models.TextField(help_text="Address Line 2", blank=True)
    zipcode = models.TextField(help_text="ZIP/PIN Code", blank=True)

    class Meta:
        ordering = ('id', )

    @property
    def state(self):
        if self.city is None:
            return None
        return self.city.state

    @property
    def country(self):
        if self.state is None:
            return None
        return self.state.country
