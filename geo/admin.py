from django.contrib import admin
from geo.models import Country, State, City, Address

admin.site.register(
    Country,
    list_display=("id", "name", "code2"),
    search_fields=("id", "name", "code2"))

admin.site.register(
    State,
    list_display=("id", "name", "country"),
    search_fields=("id", "name", "country__name"),
    raw_id_fields=("country", ))

admin.site.register(
    City,
    list_display=("id", "name", "state"),
    search_fields=("id", "name", "state__name"),
    raw_id_fields=("state", ))

admin.site.register(
    Address,
    list_display=("id", "line_one", "city", "zipcode"),
    search_fields=("id", "city__name"),
    raw_id_fields=("city", ))
