"""casemighty URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path

from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets, permissions
from casemighty.doc_generator import schema_view

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

urlpatterns = [    
    path('admin/', admin.site.urls),
    path('geo/', include('geo.urls'), name='geo'),
    # path('rest-auth/', include('rest_framework.urls')),
   # path('', include(router.urls)),
    path("docs/swagger/", schema_view.with_ui("swagger"), name="docs-swagger"),
    path("docs/redoc/", schema_view.with_ui("redoc"), name="docs-redoc"),
    path("rest-auth/", include("rest_auth.urls")),
    path("rest-auth/registration/", include("rest_auth.registration.urls")),
    path('case/', include('case.urls'), name='cases'),
    path('users/', include('users.urls'), name = 'users'),
]



