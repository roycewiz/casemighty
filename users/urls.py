from django.urls import path, include
from rest_framework import routers
from users.views import LawyerProfileView

app_name = 'users'


def get_router():
    router = routers.DefaultRouter()
    router.register('lawyerprofile', LawyerProfileView, base_name="lawyerprofile")
    # router.register('consultant', Consultant)
    # router.register('feedback', FeedbackView)
    return router


urlpatterns = [path("", include(get_router().urls))]

