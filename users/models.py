from django.db import models
from uscore.model_mixins import UserLogMixin


# Create your models here.
class BaseProfile(UserLogMixin):
    """
    Base Profile Model for both Professional and Personal Client Profiles
    """

    user = models.OneToOneField(
        "auth.User",
        on_delete=models.CASCADE,
        related_name="%(class)s",
        null=False)
    manager = models.ForeignKey("auth.User",
        related_name="manager",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        default=None)
    
    # avatar = models.FileField(
    #     storage=S3Boto3Storage(),
    #     help_text="Field to store the profile image.",
    #     upload_to=s3_profile_image_storage_path,
    #     null=True,
    #     blank=True,
    #     default=None)

    class Meta:
        abstract = True
        ordering = ('id', )

    def __str__(self):
        return self.user.email


class LawyerProfile(BaseProfile):
	"""
	Additional user profile details
	"""
	
	is_manager = models.BooleanField(
		default=False)
	is_trial = models.BooleanField(
		default=True)
	membership_upto = models.DateTimeField(
		null=True,
    	blank=True,
    	default=None)
	organisation_name = models.TextField(
    	max_length=300,
    	null=True,
    	blank=True,
    	default=None)

	class Meta:
	 	ordering = ('id', )

	def __str__(self):
		return self.organisation_name
		
    
    