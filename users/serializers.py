from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.http import Http404
from django_currentuser.middleware import get_current_user
from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from users.models import (
    BaseProfile,
    LawyerProfile)

User = get_user_model()

class BasicUserSerializer(WritableNestedModelSerializer):
    
    class Meta:
        model = User
        exclude = [
            "password", "is_superuser", "is_staff", "groups",
            "user_permissions"
        ]

    def get_email_verified(self, user):
        return user.emailaddress_set.first().verified


class LawyerProfileSerializer(serializers.ModelSerializer):
    # address = AddressSerializer(required=False)
    # phone = PhoneSerializer(source="phone_as_professional", read_only=True)
    # education = EducationSerializer(many=True, required=False)
    # verified = serializers.BooleanField(read_only=True)
    # bank_account = BankAccountSerializer(required=False, allow_null=True)

    class Meta:
        model = LawyerProfile
        exclude = []

    # def validate(self, attrs):
    #     attrs = super(ProfessionalProfileSerializer, self).validate(attrs)
    #     # if "professional_reviews" in attrs and attrs[
    #     #         "professional_reviews"] is not None:
    #     #     if not type(attrs["professional_reviews"]) == list:
    #     #         raise serializers.ValidationError({
    #     #             "professional_reviews":
    #     #             ["professional_reviews must be a list of dict"]
    #     #         })

    #     #     for each in attrs["professional_reviews"]:
    #     #         serializer = ProfessionalReviewSerializer(data=each)
    #     #         if not serializer.is_valid():
    #     #             raise serializers.ValidationError(serializer.errors)
    #     return attrs


class UserSerializer(WritableNestedModelSerializer):
    lawyer = LawyerProfileSerializer(source="lawyerprofile")
    # professional = ProfessionalProfileSerializer(source="professionalprofile")
    # email_verified = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = [
            "password", "is_superuser", "is_staff", "groups",
            "user_permissions"
        ]

    def get_email_verified(self, user):
        return user.emailaddress_set.first().verified



