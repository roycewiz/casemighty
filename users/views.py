from django.http import Http404
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, response, status, views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated

from users.models import (
	BaseProfile, LawyerProfile)
from users.serializers import (
    BasicUserSerializer, LawyerProfileSerializer)

# Create your views here.

class LawyerProfileView(mixins.CreateModelMixin,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
    viewsets.GenericViewSet):
    """
    Lawyer Profile View.
    """
    queryset = LawyerProfile.objects.all()
    serializer_class = LawyerProfileSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return self.queryset.get(pk=self.kwargs["pk"])
        if hasattr(self.request.user, "lawyerprofile"):
             return self.request.user.lawyerprofile
        raise Http404
