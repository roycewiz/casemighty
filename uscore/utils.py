import hashlib
import json
import requests
from collections import OrderedDict
from time import strftime

from django.conf import settings
from django.core.paginator import Paginator
from django.db import transaction
from rest_framework.authentication import TokenAuthentication
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param

#from wizcounsel.celery import app


# def generate_otp_code():
#     """
#     Generate a 4 digit number string based on timestamp.
#     """
#     time_stamp = strftime('%Y%m%d%H%M%S').encode('utf-8')
#     hash_code = hashlib.sha1()
#     hash_code.update(time_stamp)
#     return int(str(int(hash_code.hexdigest(), 16))[:4])


# def chunked_iterator(queryset, chunk_size=25):
#     """
#     Instead of loading the complete queryset in memory,
#     loads the paginated part of queryset.
#     """
#     paginator = Paginator(queryset, chunk_size)
#     for page in range(1, paginator.num_pages + 1):
#         for obj in paginator.page(page).object_list:
#             yield obj


# def html_to_pdf(html):
#     return requests.post(
#         settings.WKHTMLTOPDF_API_URL, data={
#             "html": html
#         }).content


class StandardResultsSetPagination(PageNumberPagination):
    page_size_query_param = 'limit'

    def get_current_link(self):
        if not self.page.has_next():
            return None
        url = self.request.build_absolute_uri()
        page_number = self.page.number
        return replace_query_param(url, self.page_query_param, page_number)

    def get_paginated_response(self, data):
        return Response(
            OrderedDict([('count', self.page.paginator.count),
                         ('next', self.get_next_link()),
                         ('current', self.get_current_link()),
                         ('currentPage', self.page.number),
                         ('previous', self.get_previous_link()),
                         ('results', data)]))


class BearerTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'


# class TransactionAwareTask(app.Task):
#     """
#     Task class which is aware of django db transactions and only executes tasks
#     after transaction has been committed
#     """
#     abstract = True

#     def apply_async(self, *args, **kwargs):
#         """
#         Unlike the default task in celery, this task does not return an async
#         result
#         """
#         transaction.on_commit(lambda: super(TransactionAwareTask, self).apply_async(*args, **kwargs))
