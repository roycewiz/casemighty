from django.core.exceptions import ValidationError
#from phonenumber_field.serializerfields import PhoneNumberField
from rest_auth.registration.serializers import \
    RegisterSerializer as BaseRegisterSerializer
from rest_auth.serializers import \
    PasswordResetSerializer as BasePasswordResetSerializer
from rest_framework import serializers

#from users.models import Phone, ProfessionalProfile

class BooleanResponseSerializer(serializers.Serializer):
    success = serializers.BooleanField()


class PasswordResetSerializer(BasePasswordResetSerializer):
    """
    Custom version of rest-auth's PasswordResetSerializer that 
    checks email validity and also sets the template for email
    """

    def get_email_options(self):
        """Overridden to support html template"""
        return {
            'html_email_template_name':
            'registration/password_reset_email_html.html'
        }


class RegisterSerializer(BaseRegisterSerializer):
    """
    Custom version of rest-auth's RegisterSerializer that 
    receives firstname and lastname from the user during registration
    """

    first_name = serializers.CharField()
    last_name = serializers.CharField()
    # is_professional = serializers.BooleanField()
    # phone = PhoneNumberField(required=False)

    def validate(self, data):
        data = super(RegisterSerializer, self).validate(data)
        # self.is_professional = data.get("is_professional", False)
        # self.phone = data.get("phone", None)
        return data

    def get_cleaned_data(self):
        context = super(RegisterSerializer, self).get_cleaned_data()
        context.update({
            "first_name": self.validated_data["first_name"],
            "last_name": self.validated_data["last_name"]
        })
        return context

    def save(self, *args, **kwargs):
        user = super(RegisterSerializer, self).save(*args, **kwargs)
        # if self.is_professional:
        #     user.clientprofile.is_default_professional = True
        #     user.clientprofile.save()
        #     professional_profile = ProfessionalProfile.objects.create(
        #         user=user)
        # if self.phone:
        #     if not self.is_professional:
        #         Phone.objects.create(
        #             client_profile=user.clientprofile, phone=self.phone)
        #     else:
        #         Phone.objects.create(
        #             professional_profile=professional_profile,
        #             phone=self.phone)
        return user