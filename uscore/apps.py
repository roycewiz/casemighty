from django.apps import AppConfig


class UscoreConfig(AppConfig):
    name = 'uscore'
