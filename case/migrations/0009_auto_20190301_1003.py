# Generated by Django 2.0.13 on 2019-03-01 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('case', '0008_timeline'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='stage',
            field=models.CharField(choices=[('Lead', 'Lead'), ('In Progress', 'In Progress'), ('Completed', 'Completed'), ('In Dispute', 'In Dispute'), ('Closed', 'Closed'), ('Migrated', 'Migrated')], default='Lead', max_length=20),
        ),
    ]
