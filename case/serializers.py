from case.models import *
from drf_writable_nested import WritableNestedModelSerializer


class CaseSerializer(WritableNestedModelSerializer):

    # job_question = CreateJobQuestionSerializer(many=True)
    # user_detail = FeedUserSerializer(source='user', read_only=True)
    # feedback = FeedbackSerializer(
    #     source="job_feedback", many=True, read_only=True)
    # document = serializers.FileField(read_only=True, required=False)
    # document_base64 = serializers.CharField(write_only=True, required=False)
    # city_data = CitySerializer(source="city", read_only=True)

    class Meta:
        model = Case
        exclude = []

    # def validate(self, attrs):
    #     attrs = super(JobSerializer, self).validate(attrs)
    #     if "status" in attrs and attrs["status"] not in [
    #             Job.STATUS_CLOSED, Job.STATUS_IN_DISPUTE
    #     ]:
    #         raise serializers.ValidationError({
    #             "status": [
    #                 "Only [%s, %s] values are allowed" %
    #                 (Job.STATUS_CLOSED, Job.STATUS_IN_DISPUTE)
    #             ]
    #         })
    #     return attrs

class TimelineSerializer(WritableNestedModelSerializer):

    class Meta:
        model = Timeline
        exclude = []

class ClientSerializer(WritableNestedModelSerializer):

    class Meta:
        model = Client
        exclude = []
    