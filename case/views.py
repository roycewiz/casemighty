from django.shortcuts import render,get_object_or_404
# Create your views here.
from django.http import HttpResponse, JsonResponse, Http404
from .models import Case, Timeline, Client
from django.views.generic import View
from rest_framework import generics,serializers, mixins, viewsets
from django.contrib.auth.models import User
from .serializers import CaseSerializer,TimelineSerializer, ClientSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated

# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")

class CaseView(mixins.CreateModelMixin,mixins.ListModelMixin,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
    viewsets.GenericViewSet):
    """
    Case details View.
    """
    queryset = Case.objects.all()
    serializer_class = CaseSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return self.queryset
        return self.queryset.filter(manager=self.request.user.lawyerprofile.manager)


class TimelineView(mixins.CreateModelMixin,
	#mixins.ListModelMixin,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
    mixins.DestroyModelMixin, viewsets.GenericViewSet):
    """
    Case details View.
    """
    queryset = Timeline.objects.all()
    serializer_class = TimelineSerializer
    permission_classes = (IsAuthenticated, )




class TimelineCaseView(mixins.ListModelMixin,viewsets.GenericViewSet):
    queryset = Timeline.objects.all()
    serializer_class = TimelineSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return self.queryset
        return self.queryset.filter(
            case=self.kwargs['caseId'])

    def get_object(self):
        return self.queryset.get(pk=self.kwargs['pk'])


class ClientView(mixins.CreateModelMixin,
	#mixins.ListModelMixin,
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
    mixins.DestroyModelMixin, viewsets.GenericViewSet):
    """
    Case details View.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated, )



