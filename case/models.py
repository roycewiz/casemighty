from django.db import models
from uscore.model_mixins import UserLogMixin
from .fields import INPanNumberField,INGstNumberField,INUIDAINumberField

from storages.backends.s3boto3 import S3Boto3Storage
from case.utils import s3_case_file_location_path

class Case(UserLogMixin):
    """
    Case basic details
    TODO add court fields like court
    and case type and all
    """

    STAGE_LEAD = "Lead"
    STAGE_IN_PROGRESS = "In Progress"
    STAGE_COMPLETED = "Completed"
    STAGE_IN_DISPUTE = "In Dispute"
    STAGE_CLOSED = "Closed"
    STAGE_MIGRATED = "Migrated"
    STAGE_CHOICES = (
        (STAGE_LEAD, STAGE_LEAD),
        (STAGE_IN_PROGRESS, STAGE_IN_PROGRESS),
        (STAGE_COMPLETED, STAGE_COMPLETED),
        (STAGE_IN_DISPUTE, STAGE_IN_DISPUTE),
        (STAGE_CLOSED, STAGE_CLOSED),
        (STAGE_MIGRATED, STAGE_MIGRATED),
    )

    title = models.CharField(max_length=200)
    description = models.TextField(blank=True,null=True)
    is_listed = models.BooleanField(default=False)
    is_wizcounsel = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    client_name = models.CharField(max_length=200)
    stage = models.CharField(
        max_length=20, choices=STAGE_CHOICES, default=STAGE_LEAD)
    manager = models.ForeignKey(
        'users.LawyerProfile',
        related_name='case_manager',
        on_delete=models.CASCADE)

    def __str__(self):
        return self.title or ""

    class Meta:
        ordering = ('id', )


class Timeline(UserLogMixin):
    """
    Timeline item basic details
    TODO add document support
    """
    
    info = models.CharField(max_length=200)
    description = models.TextField(blank=True,null=True)
    timeline_date = models.DateTimeField()
    case = models.ForeignKey(
        'case.Case',
        related_name='associated_case',
        on_delete=models.CASCADE)
    # document = models.FileField(
    #     storage=S3Boto3Storage(),
    #     help_text="Field to store the document of Timeline",
    #     upload_to=s3_case_file_location_path,
    #     null=True,
    #     blank=True,
    #     default=None)
    # document_file_name = models.CharField(max_length=100, default=True,null=True)

    def __str__(self):
        return self.info or ""

    class Meta:
        ordering = ('id', )


class Client(UserLogMixin):
    """
    Client Basic details
    """
    
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    company_name = models.CharField(max_length=100,null=True,default=None)
    mobile = models.CharField(max_length=10)
    alternate_phone = models.CharField(max_length=10,null=True)
    gst = INGstNumberField(null=True,max_length=15)
    pan = INPanNumberField(max_length=10,null=True)
    uidai = INUIDAINumberField(max_length=12,null=True)
    address = models.ForeignKey(
        'geo.Address',
        null=True,
        related_name='address',
        on_delete=models.CASCADE)

    def __str__(self):
        return self.first_name
        


