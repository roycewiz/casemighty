from django.urls import path, include
from rest_framework import routers
from case.views import CaseView, TimelineView, TimelineCaseView, ClientView

def get_router():
    router = routers.DefaultRouter()
    router.register('case', CaseView, base_name="case")
    router.register('timeline', TimelineView, base_name="timeline")
    router.register('client', ClientView, base_name='client')
    # router.register('feedback', FeedbackView)
    return router


urlpatterns = [path("", include(get_router().urls))]

urlpatterns += [
    path(
        'timeline-case/<int:caseId>',
        TimelineCaseView.as_view({
            'get': 'list'
        }),
        name='timeline-case')
]

