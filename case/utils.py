def get_user_full_name(user):
    return user.first_name + " " + user.last_name


def s3_case_file_location_path(instance, filename):
    return "case_document/{}/{}".format(str(instance.pk), filename)


# def get_counter_party_from_job(job, user):
#     if job.user == user:
#         return job.in_progress.proposal.sender.user
#     return job.user


# def get_counter_party_from_proposal(proposal, user):
#     if proposal.sender.user == user:
#         return proposal.job.user
#     return proposal.sender.user


# def user_has_access_to_case(case, user):
#     if case.manager == user.law or job.in_progress.proposal.sender.user == user:
#         return True
#     return False
