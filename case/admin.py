from django.contrib import admin

from .models import Case, Timeline, Client

admin.site.register(Case)
admin.site.register(Client)
admin.site.register(Timeline)
